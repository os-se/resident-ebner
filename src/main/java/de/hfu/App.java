package de.hfu;

import java.util.Scanner;

/**
 * Hauptklasse des Praktikumprojekts
 */
public class App {

    public static void main( String[] args ) {
        Scanner myScanner = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Enter Something: ");

        String tempString = myScanner.nextLine();  // Read user input
        System.out.println("In uppercase: " + tempString.toUpperCase());  // Output user input

        myScanner.close();
    }
}