package de.hfu.unit;

public class Util {

	public static boolean istErstesHalbjahr(int monat) throws IllegalArgumentException {
		if ((monat < 1) || (monat > 12)) throw new IllegalArgumentException();
        return monat <= 6;
        //different comment
    }
}
