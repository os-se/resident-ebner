package de.hfu.unit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestUtil {

    @Test
    public void testMonatSechsIstImErstenHalbjahr(){
        Assertions.assertTrue(Util.istErstesHalbjahr(6));
    }

    @Test
    public void testMonatEinsIstImErstenHalbjahr(){
        assertTrue(Util.istErstesHalbjahr(1));
    }

    @Test
    public void testMonatSiebenIstNichtImErstenHalbjahr(){
        assertFalse(Util.istErstesHalbjahr(7));
    }

    @Test
    public void testMonatZwoelfIstNichtImErstenHalbjahr(){
        assertFalse(Util.istErstesHalbjahr(12));
    }

    @Test
    public void testExceptionThrownBeiMonatGleichNull(){
        assertThrows(IllegalArgumentException.class, () -> {
            Util.istErstesHalbjahr(0);
        });
    }

    @Test
    public void testExceptionThrownBeiMonatGleich13(){
        assertThrows(IllegalArgumentException.class, () -> {
            Util.istErstesHalbjahr(13);
        });
    }
}
