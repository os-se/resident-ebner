package de.hfu.unit;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestQueue {
    
    @Test
    public void testCorrectOrderEnqueueAndDequeue(){
        Queue sut = new Queue(3);

        sut.enqueue(1);
        sut.enqueue(2);
        sut.enqueue(3);

        assertEquals(sut.dequeue(), 1);
        assertEquals(sut.dequeue(), 2);
        assertEquals(sut.dequeue(), 3);
    }

    @Test
    public void testOverwriteOnceCorrect(){
        Queue sut = new Queue(3);

        sut.enqueue(1);
        sut.enqueue(2);
        sut.enqueue(3);
        sut.enqueue(4);

        assertEquals(sut.dequeue(), 1);
        assertEquals(sut.dequeue(), 2);
        assertEquals(sut.dequeue(), 4);
    }

    @Test
    public void testOverwriteTwiceCorrect(){
        Queue sut = new Queue(3);

        sut.enqueue(1);
        sut.enqueue(2);
        sut.enqueue(3);
        sut.enqueue(4);
        sut.enqueue(5);

        assertEquals(sut.dequeue(), 1);
        assertEquals(sut.dequeue(), 2);
        assertEquals(sut.dequeue(), 5);
    }

    @Test
    public void testCorrectOrderDequeueAndNewEnqueue(){
        Queue sut = new Queue(3);

        sut.enqueue(1);
        sut.enqueue(2);
        sut.enqueue(3);
        sut.dequeue();
        sut.enqueue(4);

        assertEquals(sut.dequeue(), 2);
        assertEquals(sut.dequeue(), 3);
        assertEquals(sut.dequeue(), 4);
    }

    @Test
    public void testExceptionThrownDequeueWhenQueueEmpty(){
        Queue sut = new Queue(3);
        assertThrows(IllegalStateException.class, sut::dequeue);
    }

    @Test
    public void testExceptionThrownWhenMaxQueueLengthIsZero(){
        assertThrows(IllegalArgumentException.class, () -> {
            new Queue(0);
        });
    }
}
