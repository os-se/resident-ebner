package de.hfu.integration.Repository;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;

import java.util.ArrayList;
import java.util.List;

public class ResidentRepositoryStub implements ResidentRepository {

    private ArrayList<Resident> residentList = new ArrayList<>();
    @Override
    public List<Resident> getResidents() {
        return residentList;
    }

    public void addResident(Resident resident){
        this.residentList.add(resident);
    }
}
