package de.hfu.integration.Repository;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.service.BaseResidentService;
import de.hfu.integration.service.ResidentServiceException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class TestResidentRepository {

    private static BaseResidentService brService;
    private static SimpleDateFormat dateFormat;

    @BeforeAll
    public static void init() throws ParseException {
        ResidentRepositoryStub rrStub = new ResidentRepositoryStub();
        brService = new BaseResidentService();
        brService.setResidentRepository(rrStub);
        dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        rrStub.addResident(new Resident("Hans", "Maier", "Hauptstraße",
                "Hamburg", dateFormat.parse("01-02-1965")));
        rrStub.addResident(new Resident("Sebastian", "Meisinger", "Nebenstraße",
                "Berlin", dateFormat.parse("03-04-1985")));
        rrStub.addResident(new Resident("Max", "Mustermann", "Dorfstraße",
                "Furtwangen", dateFormat.parse("05-06-1975")));
        rrStub.addResident(new Resident("Max", "Mutzke", "Haupstraße",
                "Berlin", dateFormat.parse("07-08-1987")));
        rrStub.addResident(new Resident("Max", "Herre", "Hebsackstraße",
                "Freiburg", dateFormat.parse("09-10-1982")));
        rrStub.addResident(new Resident("Martin", "Maier", "Kurfürstendamm",
                "Berlin", dateFormat.parse("11-12-1955")));
    }

    @Test
    public void testResidentRepositoryReturnsAllFamilyNameEqualsMaier(){
        Resident resFilter = new Resident(null, "Maier", null,
                null, null);
        List<Resident> filteredList = brService.getFilteredResidentsList(resFilter);
        Assertions.assertEquals(2, filteredList.size());
        for (Resident resident : filteredList) {
            Assertions.assertEquals("Maier", resident.getFamilyName());
        }
    }

    @Test
    public void testResidentRepositoryReturnsAllFamilyNamesWthM(){
        Resident resFilter = new Resident(null, "M*", null,
                null, null);
        List<Resident> filteredList = brService.getFilteredResidentsList(resFilter);
        Assertions.assertEquals(5, filteredList.size());
        for (Resident resident : filteredList) {
            Assertions.assertTrue(resident.getFamilyName().matches("M.*"));
        }
    }

    @Test
    public void testResidentRepositoryReturnsAllGivenNamesEqualsMax(){
        Resident resFilter = new Resident("Max", null, null,
                null, null);
        List<Resident> filteredList = brService.getFilteredResidentsList(resFilter);
        Assertions.assertEquals(3, filteredList.size());
        for (Resident resident : filteredList) {
            assertEquals("Max", resident.getGivenName());
        }
    }

    @Test
    public void testResidentIsInRepositoryHansMaier() throws ParseException, ResidentServiceException {
        Resident testRes = new Resident("Hans", "Maier", "Hauptstraße",
                "Hamburg", dateFormat.parse("01-02-1965"));
        Resident foundRes = brService.getUniqueResident(testRes);
        Assertions.assertTrue(testRes.equals(foundRes));
    }

    @Test
    public void testResidentIsInRepositoryMaxMustermann() throws ParseException, ResidentServiceException {
        Resident testRes = new Resident("Max", "Mustermann", "Dorfstraße",
                "Furtwangen", dateFormat.parse("05-06-1975"));
        Resident foundRes = brService.getUniqueResident(testRes);
        Assertions.assertTrue(testRes.equals(foundRes));
    }

    @Test
    public void testResidentIsNotFoundJuergenHoeller() throws ParseException {
        Resident testRes = new Resident("Juergen", "Hoeller", "Hauptstraße",
                "Hamburg", dateFormat.parse("01-02-1965"));
        assertThrows(ResidentServiceException.class, () -> {
            brService.getUniqueResident(testRes);
        });
    }
}
