package de.hfu.integration.Repository;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;
import de.hfu.integration.service.BaseResidentService;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import static org.easymock.EasyMock.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class TestResidentRepositoryEasyMock {

    @Test
    public void testResidentRepositoryReturnsAllFamilyNameEqualsMaier() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        List<Resident> resList = new ArrayList<Resident>();
        resList.add(new Resident("Hans", "Maier", "Hauptstraße",
                "Hamburg", dateFormat.parse("01-02-1965")));
        resList.add(new Resident("Sebastian", "Meisinger", "Nebenstraße",
                "Berlin", dateFormat.parse("03-04-1985")));
        resList.add(new Resident("Max", "Mustermann", "Dorfstraße",
                "Furtwangen", dateFormat.parse("05-06-1975")));
        resList.add(new Resident("Max", "Mutzke", "Haupstraße",
                "Berlin", dateFormat.parse("07-08-1987")));
        resList.add(new Resident("Max", "Herre", "Hebsackstraße",
                "Freiburg", dateFormat.parse("09-10-1982")));
        resList.add(new Resident("Martin", "Maier", "Kurfürstendamm",
                "Berlin", dateFormat.parse("11-12-1955")));

        ResidentRepository resRepoMock = createMock(ResidentRepository.class);
        expect(resRepoMock.getResidents()).andReturn(resList);

        BaseResidentService resService = new BaseResidentService();
        resService.setResidentRepository(resRepoMock);

        replay(resRepoMock);

        Resident resFilter = new Resident(null, "Maier", null,
                null, null);
        List<Resident> filteredList = resService.getFilteredResidentsList(resFilter);
        assertThat(2, equalTo(filteredList.size()));
        for (Resident resident : filteredList) {
            assertThat("Maier", equalTo(resident.getFamilyName()));
        }

        verify(resRepoMock);
    }
}
