<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>de.hfu</groupId>
    <artifactId>jonasebner_uebung5_opensource</artifactId>
    <version>1.0-SNAPSHOT</version>

    <name>jonasebner_uebung5_opensource</name>

    <!--======================================================================
      Mit dem properties-Element können einige Werte definiert werden,
      die in dieser pom.xml als Variablen verwendet werden können.
    ======================================================================= -->
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>${maven.compiler.source}</maven.compiler.target>

        <junit.version>5.9.1</junit.version>
        <jacoco.version>0.8.8</jacoco.version>
        <easymock.version>5.0.1</easymock.version>
        <hamcrest.version>2.2</hamcrest.version>
        <maven.surefire.version>2.22.2</maven.surefire.version>
    </properties>


<!--======================================================================
  Mit dem dependencies-Element können Bibliotheken definiert werden,
  die bei der Erstellung des Projekts heruntergeladen und verwendet werden
  sollen.
======================================================================= -->
    <dependencies>
        <!--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
          Bibliothek für die Unit-Tests
        :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter</artifactId>
            <scope>test</scope>
        </dependency>

        <!--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
          Bibliothek für das Erstellen von Mock-Objekten (werden verwendet in
          Unit-Tests)
                  :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->
        <dependency>
          <groupId>org.easymock</groupId>
          <artifactId>easymock</artifactId>
          <version>${easymock.version}</version>
          <scope>test</scope>
        </dependency>

        <!--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
          Bibliothek für Assert-Matcher (werden verwendet in Unit-Tests)
                  :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-library</artifactId>
            <version>${hamcrest.version}</version>
            <scope>test</scope>
        </dependency>

    </dependencies>

    <!--======================================================================
      Mit dem reporting-Element können Plugins definiert werden,
      die bei der Erstellung einer Projekt-Webseite Inhalte beitragen.
      Die Projekt-Webseite ist unabhängig vom Bauen des Projekt-Pakets
      und muss mit "mvn site" angestoßen werden. Die fertige Projekt-
      Webseite kann dann unter target/site mit einem Webbrowser angeschaut
      werden.
    ======================================================================= -->
    <reporting>
        <plugins>
            <!--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
              Dies ist das Standard-Reporting-Plugin, welches Daten
              aus dieser Konfigurationsdatei verwendet, um diese auf
              der Projekt-Webseite darzustellen.
            :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->
            <plugin>
              <artifactId>maven-project-info-reports-plugin</artifactId>
            </plugin>

            <!-- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
              Das surefire-Reporting-Plugin erstellt Webseiten aus den Ergebnissen
              von Unit-Tests und bindet diese in der Projekt-Webseite ein.
            :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->
            <plugin>
                <artifactId>maven-surefire-report-plugin</artifactId>
            </plugin>

            <!-- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
              Das jacoco-Reporting-Plugin stellt die Code-Coverage der Unit-Tests dar
              und bindet diese in der Projekt-Webseite ein.
                          :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->
            <plugin>
              <groupId>org.jacoco</groupId>
              <artifactId>jacoco-maven-plugin</artifactId>
              <version>${jacoco.version}</version>
            </plugin>

        </plugins>
    </reporting>

    <!--======================================================================
      Mit dem build-Element können Plugins definiert werden, die die Standard-
      Aktionen von Maven um eigene Aktionen erweitern. Kompilieren, Testen
      und Paketieren brauchen Sie hier nicht mehr zusätzlich anzugeben.
    ======================================================================= -->
    <build>
        <plugins>

            <!-- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
              Das maven-jar-Plugin erstellt aus Ihrem Projekt eine Jar-Bibliothek.
              Um die Klasse mit der main-Methode zu definieren, müssen Sie unten bei
              mainClass den Namen der Klasse mit Package aber ohne Dateiendung
              angeben.
                          :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <configuration>
                    <archive>
                        <manifest>
                            <addClasspath>true</addClasspath>
                            <mainClass>de.hfu.App</mainClass>
                        </manifest>
                    </archive>
                </configuration>
            </plugin>

            <!-- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
              Das jacoco-Plugin führt die Code-Coverage-Analyse während der Unit-Tests
              durch.
                          :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->
            <plugin>
              <groupId>org.jacoco</groupId>
              <artifactId>jacoco-maven-plugin</artifactId>
              <version>${jacoco.version}</version>
              <executions>
                <execution>
                  <id>default-prepare-agent</id>
                  <goals>
                    <goal>prepare-agent</goal>
                  </goals>
                </execution>
                <execution>
                  <id>default-report</id>
                  <goals>
                    <goal>report</goal>
                  </goals>
                </execution>
              </executions>
            </plugin>
        </plugins>

        <!-- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
          Das pluginManagement-Element definiert die zu verwendenden Versionen
          der Standard-Maven-Plugins. Nicht zwingend notwendig, aber so bekommen
          wir eine definierte Build-Umgebung.
        :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->
        <pluginManagement>
            <plugins>
                <plugin>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>3.10.1</version>
                </plugin>
                <plugin>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>${maven.surefire.version}</version>
                </plugin>
                <plugin>
                    <artifactId>maven-surefire-report-plugin</artifactId>
                    <version>${maven.surefire.version}</version>
                </plugin>
                <plugin>
                    <artifactId>maven-jar-plugin</artifactId>
                    <version>3.3.0</version>
                </plugin>
                <plugin>
                    <artifactId>maven-site-plugin</artifactId>
                    <version>3.12.1</version>
                </plugin>
                <plugin>
                    <artifactId>maven-project-info-reports-plugin</artifactId>
                    <version>3.4.1</version>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <!--======================================================================
      Mit dem dependencyManagement-Element können wir festlegen, welche
      Versionen Maven für Abhängigkeiten verwendet. Erspart uns Schreibarbeit,
      da wir die Versionen oben im dependencies-Element nicht mehr angeben
      müssen.
    ======================================================================= -->
    <dependencyManagement>
        <dependencies>
            <dependency>
               <groupId>org.junit</groupId>
               <artifactId>junit-bom</artifactId>
               <version>${junit.version}</version>
               <type>pom</type>
               <scope>import</scope>
           </dependency>
            <!--<dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <version>4.13.2</version>
                <scope>test</scope>
            </dependency>-->
        </dependencies>
   </dependencyManagement>

   <developers>
       <developer>
           <id>some_id</id>
           <name>somebody</name>
           <email>example@mail.com</email>
           <roles>
               <role>Heiopei</role>
               <role>Pfuscher</role>
           </roles>
           <url>http://www.some_site.com</url>
           <organization>Some Organization</organization>
           <organizationUrl>http://www.some_organization.com</organizationUrl>
           <properties>
               <picUrl>data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUSExIVFRUVFxUVFRUVFRUVFRUVFRUXFxUVFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OFxAQFSsdFR0tKy0tLS0rLS0tLS0tLS0tLS0rLS0tKy0tLS0tLS03LS0tKystNy03LS0rLSstLSsrN//AABEIAOEA4QMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAACAAEDBAUGBwj/xAA3EAACAQIFAwIEBAUDBQAAAAAAAQIDEQQFEiExBkFhUXETIjKRgaGxwQcUQlLRIyTxFTNysvD/xAAYAQADAQEAAAAAAAAAAAAAAAAAAQIDBP/EACARAQEAAgMAAgMBAAAAAAAAAAABAhEDITETQRJRYTL/2gAMAwEAAhEDEQA/AOdQFSwbYMzgd6CVgduwUhgB0wVFsNWEBmjEdsaTATYATYhRRJCN+BkLDYeU3aMW77bepJisHOlLTOLT5+/B6Z0b07OEYzcOVez9HvdeToMz6Vo1papc2S/BHROHpheXt4phsJUq30RbtzbsVsRRkuUfQOBymhSh8OEEl323fuzJzDpyhK7cI+LrhjnDC+X+PDZRAuekZ50fGNOc4K8nuuyS8I8/rUXBtSTT8ozzwuLTDOZIIx9CZIYexk0NNlZsmqoiGVCMODIYhAsKw7iI0aiPpDYDACbI7DgsAIQNxANtwaYymNMgIpsCQ82R6mUBRCUhkgrCMpMZDND3AhxR1/SeVUqjTqNrj0szjozOg6VwTq1VeVkvq8r0NeP1Gfj2jDSjFKMXtsl7E7qL1OG66qV4YL/aKetOO1NXnpuk7eSDB47FpU6dVqFd01KScbpO1ndp2uddcjt5V4ruvuQSx8WrHjmf55m1HFqm4txlKOjTC9OUO95dn+h02VZjVnJxa9N/AeB2VSalyee9Z5LJT+JFLS7997nYzc0ldkGMpfEg01fYeUlmhjdXbyWULPdEbL2aYfRNrhX2KEmcFmrp2y7gZSIrhSj3IpMBRNgNgOYpMYg7g3BuM5CMTkJEaJUAMM4iuMwGysIVhAG0kDNhNkdRkhDJkVwptAyZRDgw0yON9grCMbkBJ7jarEUphBUkpPsb3S+IkqqWrlr7I5+O5r9OQ/3FK3OqK5+5ePqcvHr2NxGmMZLsjj63xJ1HV1uLjdRTXMfPudzOgnyUcRk8Jco6q5HMf9RlONns1yv3XgfK6r1enm25oV8tUHpikXsvyVqzY4LU+Coa3dvYlx0YwWxd0KKOUzPHy+I4p7Pgvek6cN1L/wB6T8mM0a2e0ZQqSUu+6fhmRVlscOf+q7cP8o6sys5BzkR2FAZIJisMwBIVgB2B7HYVyNMdyAbPcTkNYVwIriGuIDbrIqjE5AVJEGhqx9AUwZSGRRJ4yGlvwBFDNgZ7MZIa4gJYhJE+GruLUlyt0U4wuWacQ+x9Paun8f8AHown3aWr3NWVLYyOh8n+Dh4avqmlJ+L7pfmdNKKO2eOPKdsaGG+ZNlqU7FivZLYqyt3KiWdmVZtWRgPBb6mdJiIpvZGfjqDaGHP5plf8zSlGNtcd4+fFzzjFUnGTjJWa2afZ+h7PlGGsm2cR/ETKdMvjwjs9p29ezZlyYbm41489XThpEcmh5MicjmdB3Ij1CY8YADoVxSuhhAQhkFJACuMK4mAPYQ1hgNsWIqzJEQ1pEw6juhtVwO4aRSRt7EbYpDCM6CiKA9gCam1ez/VnbdCZHSxFS8ovTGz3b3t4OKpQX/yR69/DynGNFtLfa78mvFN1ny3Udc1a1uFsvBWxFeyJpz2KNfc6o5U1BN7stToxSu+SrQlZFfF4t32GFiKSZSxTW4XxirXqdhgsItn7lPqXCJ0J6ldaXdepo0adkBiZqUXGXdW3D6H28CrRV37uxA1c1c/wvw604ri7t7FfK6KnUjF8N7+px2d6dUvTTyHp2VVq6dvyO0pdC01Djd8nRZJgoU4Kysa7mrHRjxyRhlyXbxDP8ldKT2ez7cWMOUT3fNKdFp64Qe3dHj/Uqh8V6LJLZWWxlyceu2nHnvpjjoawkYNj2FJjtkbAFqHAEAbOognK5I3sQyEYWEMhXGRpDo6DpPIFjfi0k9NSMdcJPj00tenkz80yevh6jp1abjJcNK8ZeYy7of40vynilBjpts0cJktSdvl2btt5NPLuicTVm4KDik/qkmo/gOYWi5SKeQZRPEVY04q1+7va3f3Pa8Jl8cPSjThwl9/JQ6X6Vp4OOpvXUtbV2X/iuxp16l+f+Do48Pxc/Jn+SKVWxB8QjnWtyA5Gu2aedQh1ohlIS3DYHOoiTLqLnKTtsrWKl7u3qb+VwUIbvy2/JOzNUoWRh5tVjFO50GOexwHVWbQjCSTV+3uVvotdvP8AqKqp1ZSvffkzcJWdOcZrmLTCrO7b9SOxy297dMnWnruVZtGrTU4vlcefQufzjON6GjL4Uv7dW3vZXOjrROjHLcYZTVQ5lVcro8/6lwumSfr2O1xlXSjz3O67lVk279jPlvS+Kds+UhrgtjnM6BMZoVh7jALDj6hAGhcCQ9wZEmC+52PSGHoqyrLDycrWjKM6tT2UY7I48u4LOKlGLjSag5fVOK+dr0Uuy9isbqpym498y/C4ejTcoQhTVrytFR+9jnc16rouooR0yt2dnd+u55Pic8xFSKhKrLStlGLtH7IoxqtO6e5p8jP4nuODzW/EIRXsv1NSlj77qS/yeVZXn8Y4aeqXz8R33u+4eD6lqQX1X97fqaTkiPjr1StmPkz62Mu7X5OBwnVEpt3u73slu/Y2Mvp4qq7qGhPvLn3S9ipybTcLG9N3aXkvLDsfLMqcEtTba7v8zRlT2KTWb/LjSoF7QNKkMlPB4e8i7muFcoxVttcdVv7b7/oKnBxa8mX1RjXplTi5KWzjKH/rL8yacFmuZpJ2Z5t1LTdSnKSX0tP8O5sappOMm3f19Ro4VTjOL/qVvuK3cVOnnD9DY6d6arYqS0rTTX1TfCXj1Z0mSfw7lJqdaoowe6jHeTXl9j0KnQhSgoU4qMYqySJw4/2vLk/TLo5dTo0404K0Yqy/dvyU8ZJI0cTVRz2Z4tJOzNfGXrG6hxqim7+xwNaq5O7NXqDGOcrdkYrObky26OPHRw7kYSM2gmCxCQgVhxWEAX0M2ElYGQlI2RhTYEUMhINIZEsICMosNNsONMu5XgHVqRpx2cnZfuHpeHyDDzlXhGCbbl/zc9xwWCcYpd7GV0l0nDCq99c3zJq1vCOsjFI6sMdRzZ5bvSOENtxp0w5ysCpGjM0aC5HlSXf7EimNNdwCjj+HbZ9n59TmK8NL3bfHPj0OkxsmYeYRuG4GHiop8EdN2JtFnYp4yi73TZJtnL8ySp8vZ2/cCrnxzOJxWlfV7mJWzxLgdzOY7dZjs0b34OYzfMtn6mZis8k9tzGxGKnLlmeWbTHA1epdtkI7YKMGw7DiUR7CBJCY6iPoGDDBiALsRqpM6ZBiOCFq0mKEQqcG3sXqOF9R7SgpUblynRsT0qSQ7JtNCqZ6f0B0+qUPjVF887OO30x/ycz0h0/KvUjOSapxd7/3Ndj1mlSSR0cWH3WPLn9RLrsBVr2I8ROxUlVTOnTmTSxIlXKzpp9zOzDHKDs3tvdgHRU6tyyuDn8kxPxGrO65/A3mxU1XGoxcTE2MY9uTGxk1YkMrFbSMvH4yMU7clvG4hb+Dm8wrqUlb3YHIwep1UbjplZNO69TBnSqLd2OmzmDvB9rMza9P5WY51vjOmRGV/wABNCStJr2D0ma0LiFGJLpGaAwokQyiEoiBJD6QkMwMtIhWEBNpUwauBUuS2giFqtPCJEmgmEwJA0FTpboksFDkc9FeqdMRUaMUlY1qlQxsjn/oQfquwWOx9kd+Pjhvps3zWFL6m7vhLuczV6qqOT004peW2/yOX6rzCU6t77JbfuYUcfUX9TMs+Sy6jbDjlm67ur1FiW/rjHwor9WY+Or16rs6rab4skv0MGObT7lnDZvaSct1dXX4mXyZNPjxewdFZV8Cj80nKUt2/wBEjbqSuZ/S+MVbDQqRjZS4XhOxoVdjo25lLG1OxzuaVtreprYqtdvwctnGKSTQgx82xFot32RyVfOk5Wpq7XL7Ir9XZi3KNJSsnfVb09CrlsKcERldNccW3/MzmlqeyXANVbAU8VT9RYrG04wb1K6W1+7MbbW0mmCn/qzJ7lLB1NUpS9f8ltjpC1DoBBJiMY4KCEcOkMEhJADDBiAnQJjjIchZDiGkBHEmMaWV5NUrNWsl6tjxlt6K2SO16fqt4WDfo0jPzuqlFtvZG7TwXwaEKd7uK5448HA9S4xy1K/f9zt8xcnuTnc0ldt/YyJM08a/lMqcjlt3XVJqHuOpkOoWoQe7dDZlGeEpKL+mKi/D7mvicQ2eXfwsxzVSpRvs0pJeVsz0qfB0S7jmymqzq9RJM4PqXG/DjUl3jFteWdrmLV3bseWdY49OM4X3epe5RY+uFjVlKblLl7mjRmZlDkv0mZZN8VtVCli5tonvsU8SyZFWp8r7/gaEYlDKuH+BpIWXohkgh0OIzxQSBQSEZ7DjCAj3ECIA3YzDjMoqT9R9TIWvahnIrRqDuoAbvTmVxr1dMpaYpXk1z4SPQ6SoYeOmPEVdt8/c8qwOYTpO8Hz+wOY51iKt1tFO13fey24NcM5jP6xzxuVdx1F1ZBQWneXFjisdX+IrlJPbn7gyqbDz5djHikQY9/L+JlzNDFv5fxM+ZnGtR3EhDXKS0+n81lhq8KsXaztL0cXyme4YfEqcVKLumrp+qPnpyPUug85lUwuhv5qb0+Wuxpx36Zck+27nNe2657HkPX+AUZ60/q5Xk9OzWbtv9zy/r6vK8Ivjd8GtZ4uUpcl2mylR5LkZGWTbFMypiXsWGypiRQ6vZU9n7/sXlIz8s+l+5eTJy9OJFIMiiFcRpLj3I0wkxGOI8gLjqQEcQOocA00NcDUFFkLHcFSFcEAl1gqZHNgXAJZVSKdQjkyOch6IVSezKkmSuRDJjAGAx3Ia40mkjuf4X0251I2drJ+EcOldpK+/oe59D5H/ACuHWqKVSXzS8X4Rpx+oz8SYvA7WZ57/ABIyun8HVKSjKO68+D0TO8wVNN9zx/rnMJTVpPVq3V+yN7ZphjO3FUyzTIVCxNAyreJLlfEonK+JYp6dWsBL5S5GRQwb2LcWTl6cTxkGpEEWHFkmmQWoiTCTEEo1xri1AElhAXYgDQVwySmh9BC0ekaxI4gAAzRFJ2JZEM2OAEmQzZKyGQyC2R1B5vdAzYxUTGbHkMNK/keKjTxFOpNXUZXt5/p/Ox7FHqelp2mmlFPnuzwxkscZNbatvzLxukZY7dV1N1U6slZ7RTbS7u7SOLxmIlUd5O7BkwGh3LYmMiOw6QdhhKMVq5YkyviBz0qehVtsWoVDPg9y1BjyglXoSRJBlamTRM6adSHbIlcNMRjGvYQmIC1DA3EBugiOOIzUGXBChCGAVeCtMQhwiIRCGEVTkGY4hkiDlwIQEh7gT5HEUSOfIDHEMBGEIYCytiBhDx9Ko4FuAhFZJxWaZNAcRnVpewkIRBjGkIQBGIQhh//Z</picUrl>
           </properties>
       </developer>
   </developers>

</project>